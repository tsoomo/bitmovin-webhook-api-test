const express = require('express')
const app = express()
const port = 8010

app.use(express.json())

app.post("/webhook", (req, res) => {
    console.log(req.body)
    res.send("OK");
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})